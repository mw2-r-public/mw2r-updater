# Updater

The official MW2:R CLI and GUI updaters. Find downloadable releases [here](https://gitlab.com/mw2-r-public/mw2r-updater/-/releases) or build the solutions yourself.

Requires .NET Core # (TODO: determine minimum version).
