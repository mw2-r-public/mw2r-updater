﻿using System.Net.Http;

namespace MW2R.UpdaterLib
{
    public static class HttpClientSingleton
    {
        private static HttpClient Client;

        public static HttpClient GetInstance()
        {
            if(Client == null)
            {
                Client = new HttpClient();
            }

            return Client;
        }
    }
}
