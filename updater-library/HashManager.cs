﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;

namespace MW2R.UpdaterLib
{
    public static class HashManager
    {
        private const string MAIN_SERVER_BASE = "https://mw2r-ci.emosewaj.eu";
        private const string GITLAB_API_BASE = "https://gitlab.com/api/v4";
        private const string FILE_REPO_ID = "28585602";
        private const int PER_PAGE = 20;

        public static Dictionary<string, string> GetLocalHashesFromStorage()
        {
            Dictionary<string, string> hashes = new Dictionary<string, string>();

            if (!File.Exists(PathManager.GetAppdataApplicationPath() + "localHashes.dat"))
                return hashes;

            string json = Encoding.UTF8.GetString(Convert.FromBase64String(File.ReadAllText(PathManager.GetAppdataApplicationPath() + "localHashes.dat")));
            JsonDocument jdoc = JsonDocument.Parse(json);

            foreach(JsonProperty jProp in jdoc.RootElement.EnumerateObject())
            {
                hashes.Add(jProp.Name, jProp.Value.GetString());
            }

            return hashes;
        }

        public static Dictionary<string, string> GetLocalHashesFromFiles(string baseDirectory, string[] filesToHash)
        {
            Dictionary<string, string> hashes = new Dictionary<string, string>();
            List<string> files = FileValidator.CollectFilesInPath(baseDirectory);

            files = files.Where(f =>
            {
                if (filesToHash.Length != 0)
                    return true;

                f = f.Substring(baseDirectory.Length);

                return filesToHash.Contains(f);
            }).ToList();


            foreach(string file in files)
            {
                hashes.Add(file.Substring(baseDirectory.Length), FileValidator.GetHashFromFile(file));
            }

            return hashes;
        }

        public static async Task<Dictionary<string, string>> GetHashesFromMainServer()
        {
            HttpClient client = HttpClientSingleton.GetInstance();

            string json = await client.GetStringAsync(string.Format("{0}/manifest/mw2r-downloadfiles/latest.json", MAIN_SERVER_BASE));
            JsonDocument jdoc = JsonDocument.Parse(json);

            Dictionary<string, string> hashDict = new Dictionary<string, string>();

            foreach (JsonProperty jProp in jdoc.RootElement.EnumerateObject())
            {
                hashDict.Add(jProp.Name, jProp.Value.GetString());
            }

            return hashDict;
        }

        public static async Task<Dictionary<string, string>> GetHashesFromGitLab()
        {
            List<string> paths = await BuildPathList();

            return await FetchHashesFromPaths(paths);
        }

        private static async Task<List<string>> BuildPathList()
        {
            List<string> paths = new List<string>();
            int page = 1;
            HttpClient httpClient = HttpClientSingleton.GetInstance();
            JsonDocument jdoc;

            do
            {
                string uri = string.Format("{0}/projects/{1}/repository/tree?recursive=true&per_page={2}&page={3}", GITLAB_API_BASE, FILE_REPO_ID, PER_PAGE, page);
                System.Diagnostics.Debug.WriteLine("Sending " + uri);
                string json = await httpClient.GetStringAsync(uri);
                System.Diagnostics.Debug.WriteLine("Received!");

                jdoc = JsonDocument.Parse(json);

                foreach (JsonElement jElem in jdoc.RootElement.EnumerateArray())
                {
                    // Only actual files should be saved
                    if (jElem.GetProperty("type").GetString() != "blob")
                    {
                        continue;
                    }

                    paths.Add(jElem.GetProperty("path").GetString());
                }

                page++;
            } while (jdoc.RootElement.GetArrayLength() == PER_PAGE);

            return paths;
        }

        private static async Task<Dictionary<string, string>> FetchHashesFromPaths(List<string> paths)
        {
            Dictionary<string, string> hashDict = new Dictionary<string, string>();
            HttpClient httpClient = HttpClientSingleton.GetInstance();

            foreach (string path in paths)
            {
                string uri = string.Format("{0}/projects/{1}/repository/files/{2}?ref=master", GITLAB_API_BASE, FILE_REPO_ID, HttpUtility.UrlEncode(path));

                HttpRequestMessage httpRequest = new HttpRequestMessage
                {
                    Method = HttpMethod.Head,
                    RequestUri = new Uri(uri)
                };

                System.Diagnostics.Debug.WriteLine("Sending " + uri);
                HttpResponseMessage httpResponse = await httpClient.SendAsync(httpRequest);
                System.Diagnostics.Debug.WriteLine("Received!");

                string hash = ((string[])httpResponse.Headers.GetValues("X-Gitlab-Content-Sha256"))[0];
                hashDict.Add(path, hash);
            }

            return hashDict;
        }
    }
}
