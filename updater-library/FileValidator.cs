﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;

namespace MW2R.UpdaterLib
{
    public class FileValidator
    {
        private static SHA256 SHA256 = SHA256.Create();

        public static List<string> CollectFilesInPath(string path)
        {
            List<string> files = new List<string>();

            if (!Directory.Exists(path))
                return files;

            foreach(string file in Directory.EnumerateFiles(path))
            {
                files.Add(file);
            }

            foreach(string directory in Directory.EnumerateDirectories(path))
            {
                files.AddRange(CollectFilesInPath(directory));
            }

            return files;
        }

        public static string GetHashFromFile(string path)
        {
            path = Path.GetFullPath(path);

            FileStream fileStream = File.OpenRead(path);
            byte[] computedHash = SHA256.ComputeHash(fileStream);

            return BitConverter.ToString(computedHash).Replace("-", "").ToLower();
        }

        public static bool ValidateFile(string path, string expectedHash)
        {
            path = Path.GetFullPath(path);

            FileStream fileStream = File.OpenRead(path);
            byte[] computedHash = SHA256.ComputeHash(fileStream);
            string formattedHash = BitConverter.ToString(computedHash).Replace("-", "").ToLower();

            return formattedHash == expectedHash;
        }

        public static Dictionary<string, string> GetNonMatchingHashes(Dictionary<string, string> hashes)
        {
            Dictionary<string, string> retDict = new Dictionary<string, string>();

            foreach(KeyValuePair<string, string> entry in hashes)
            {
                if(!ValidateFile(entry.Key, entry.Value))
                {
                    retDict.Add(entry.Key, entry.Value);
                }
            }

            return retDict;
        }
    }
}
