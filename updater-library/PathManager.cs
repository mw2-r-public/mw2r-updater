﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MW2R.UpdaterLib
{
    public static class PathManager
    {
        private static string appdataApplicationPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\MW2R\\Updater\";

        public static string GetAppdataApplicationPath()
        {
            return appdataApplicationPath;
        }
    }
}
