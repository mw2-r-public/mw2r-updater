﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace MW2R.CLIUpdater
{
    class Program
    {
        static async Task Main()
        {
            Console.WriteLine(@"    __  ____       _____     ____ ");
            Console.WriteLine(@"   /  |/  / |     / /__ \ _ / __ \");
            Console.WriteLine(@"  / /|_/ /| | /| / /__/ /(_) /_/ /");
            Console.WriteLine(@" / /  / / | |/ |/ // __/_ / _, _/ ");
            Console.WriteLine(@"/_/  /_/  |__/|__//____(_)_/ |_|  ");
            Console.WriteLine();

            
            if (!Directory.Exists(UpdaterLib.PathManager.GetAppdataApplicationPath()))
            {
                Directory.CreateDirectory(UpdaterLib.PathManager.GetAppdataApplicationPath());
            }

            /*
             * 1. Identify missing files in current folder
             *  a. Retrieve file hashes from remote
             *    I.  Fall back to gitlab if main server unavailable somehow
             *  b. Generate local file hashes
             *  c. Compare hashes and generate list of files to download
             * 2. Download files from GitLab
             */

            string installDir = Assembly.GetExecutingAssembly().Location;

            Console.WriteLine("MW2:R will be installed in:");
            Console.WriteLine(installDir);
            Console.WriteLine();

            Console.WriteLine("Fetching manifest...");

            Dictionary<string, string> remoteHashes;

            try
            {
                remoteHashes = await UpdaterLib.HashManager.GetHashesFromMainServer();
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("Fetching manifest failed: {0}", ex.Message));
                Console.WriteLine("Building manifest from GitLab. This may take a while, please wait patiently...");

                try
                {
                    remoteHashes = await UpdaterLib.HashManager.GetHashesFromGitLab();
                } catch (Exception ex2)
                {
                    Console.WriteLine(string.Format("Building manifest failed: {0}", ex2.Message));
                    Console.WriteLine();
                    Console.WriteLine("The updater cannot continue. Please report these errors to the MW2:R team!");

                    Console.WriteLine();
                    Console.WriteLine("Press any key to exit...");
                    Console.ReadKey();

                    return;
                }
            }

            Console.WriteLine("Obtaining local hashes...");
            Dictionary<string, string> localHashes = UpdaterLib.HashManager.GetLocalHashesFromStorage();
        }
    }
}
